# docker vagrant
This is a simple vagrant init that has docker on ubuntu trusty 64 bit as well as docker compose

# usage
[Install Vagrant](https://www.vagrantup.com/docs/installation/) then install [Virtualbox](https://www.virtualbox.org/wiki/Downloads) then clone this down and type the following:

```sh
vagrant up && vagrant ssh
```

Once in the box you should be able to do docker things and docker compose things.


# other notes
* Your current working directory when you up this repo is avaialbe in the ```/src``` directory on the vagrant machine
* Your vagrant machine's IP address is 192.168.33.10

To change these values modfiy your [Vagrantfile](Vagrantfile) and run ```vagrant reload``` from your terminal.
